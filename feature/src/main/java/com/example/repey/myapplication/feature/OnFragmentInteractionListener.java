package com.example.repey.myapplication.feature;

/**
 * Created by Andrey Solomatin on 04-Jun-18.
 */
public interface OnFragmentInteractionListener {

    void onFragmentInteraction(int count);

}
