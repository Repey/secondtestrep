package com.example.repey.myapplication.feature;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {


    private final BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            /*switch (item.getItemId()) {
                case R.id.navigation_products:
                    clearFragments();
                    changeFragment(ProductsFragment.newInstance(1));
                    return true;
                case R.id.navigation_notifications:
                    clearFragments();
                    changeFragment(NotificationsFragment.newInstance(1));
                    return true;
                case R.id.navigation_profile:
                    clearFragments();
                    changeFragment(ProfileFragment.newInstance(1));
                    return true;
                    }*/

            if (item.getItemId() == R.id.navigation_products) {
                clearFragments();
                changeFragment(ProductsFragment.newInstance(1));
                return true;
            } else if (item.getItemId() == R.id.navigation_notifications) {
                clearFragments();
                changeFragment(NotificationsFragment.newInstance(1));
                return true;
            } else if (item.getItemId() == R.id.navigation_profile) {
                clearFragments();
                changeFragment(NotificationsFragment.newInstance(1));
                return true;
            }
            return false;
    }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        changeFragment(ProductsFragment.newInstance(1));
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() == 1) {
            finish();
        } else {
            fragmentManager.popBackStack();
        }
    }

    private void changeFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void clearFragments() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
    }

    @Override
    public void onFragmentInteraction(int count) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.container);
        if (fragment instanceof ProductsFragment) {
            changeFragment(ProductsFragment.newInstance(count));
        } else if (fragment instanceof NotificationsFragment) {
            changeFragment(NotificationsFragment.newInstance(count));
        } else if (fragment instanceof ProfileFragment) {
            changeFragment(ProfileFragment.newInstance(count));
        }
    }

}



