package com.example.repey.myapplication.feature;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ProductsFragment extends Fragment {

    public static final String TAG = "ProductsFragment";
    private static final String COUNT = "count";

    private int count;
    private TextView textTitle;
    private TextView textNumber;
    private Button buttonMore;

    private OnFragmentInteractionListener listener;

    public ProductsFragment() {
        // Required empty public constructor
    }

    public static ProductsFragment newInstance(int count) {
        ProductsFragment fragment = new ProductsFragment();
        Bundle args = new Bundle();
        args.putInt(COUNT, count);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            count = getArguments().getInt(COUNT);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        textTitle = view.findViewById(R.id.text_title);
        textTitle.setText(TAG);
        textNumber = view.findViewById(R.id.text_number);
        textNumber.setText(String.valueOf(count));
        buttonMore = view.findViewById(R.id.button_more);
        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMoreButtonPressed();
            }
        });
        return view;
    }

    public void onMoreButtonPressed() {
        if (listener != null) {
            listener.onFragmentInteraction(count + 1);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

}
